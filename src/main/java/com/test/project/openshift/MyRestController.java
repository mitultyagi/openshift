package com.test.project.openshift;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

@RestController
public class MyRestController {
    private Logger logger = LoggerFactory.getLogger(MyRestController.class);

    @GetMapping("/")
    public ResponseEntity<String> getNodeName() throws UnknownHostException {
        return new ResponseEntity<>("Hello from "+ InetAddress.getLocalHost().getHostName(), HttpStatus.OK);
    }

    @GetMapping("/readFile")
    public ResponseEntity<String> getFileContents() throws UnknownHostException {
        String content = "Unable to read";
        try
        {
            content = new String ( Files.readAllBytes( Paths.get("/data/readFile.txt") ) );
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return new ResponseEntity<>(content, HttpStatus.OK);
    }


}
